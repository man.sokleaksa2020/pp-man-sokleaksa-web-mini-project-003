import React, { useContext, useEffect } from "react";
import { Navbar, NavDropdown, Nav, Form, Button, FormControl, Container } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { strings } from "../localization/localize";
import { LangContext } from "../contexts/LangContext";

export default function MyNavBar() {

  const context = useContext(LangContext)

    useEffect(() => {
      if(localStorage.getItem('lang') !== null){
        context.setLang(localStorage.getItem('lang'))
        strings.setLanguage(localStorage.getItem('lang'))
      }else{
        localStorage.setItem('lang', 'en');
        context.setLang(localStorage.getItem('lang'))
        strings.setLanguage(localStorage.getItem('lang'))
      }
  }, [context])

  const onChangeLanguage = (lang) =>{
    context.setLang(lang);
    strings.setLanguage(lang);
    localStorage.setItem('lang', lang);
  }


  return (
    <div>
      <Navbar bg="dark" variant="dark" expand="lg">
        <Container>
        <Navbar.Brand href="#"><strong>AMS Redux</strong>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="mr-auto my-2 my-lg-0"
            style={{ maxHeight: "100px" }}
            navbarScroll
          >
            <Nav.Link as={NavLink} to='/'>{strings.Home}
            </Nav.Link>
            <Nav.Link as={NavLink} to='/article'>{strings.Article}
            </Nav.Link>
            <Nav.Link as={NavLink} to='/author'>{strings.Author}
            </Nav.Link>
            <Nav.Link as={NavLink} to='/category'>{strings.Category}
            </Nav.Link>
            <NavDropdown title={strings.Language} id="navbarScrollingDropdown">
              <NavDropdown.Item onClick={()=>onChangeLanguage('kh')}>{strings.Khmer}</NavDropdown.Item>
              <NavDropdown.Item onClick={()=>onChangeLanguage('en')}>
                English
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
          <Form className="d-flex">
            <FormControl
              type="search"
              placeholder="Search"
              className="mr-2"
              aria-label="Search"
            />
            <Button
            >Search</Button>
          </Form>
        </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}
