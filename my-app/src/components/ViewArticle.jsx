import React, { useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { fetchArticleById } from "../redux/action/articleAction";

export default function ViewArticle() {
  let { id } = useParams();

  const dispatch = useDispatch();
  const state = useSelector((states) => states.articleReducer.view);

  useEffect(() => {
    dispatch(fetchArticleById(id));
  }, [dispatch,id]);


  return (
    <Container>
      {state ? (
        <>
          <Row>
            <Col md={8}>
              <h1 className="my-2">{state.title}</h1>
              <img
                width="500"
                height="100%"
                alt="..."
                src={
                  state.image
                    ? state.image
                    : "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"
                }
              />
              <p>{state.description}</p>
            </Col>
          </Row>
        </>
      ) : (
        <h1>Loading...</h1>
      )}
    </Container>
  );
}
