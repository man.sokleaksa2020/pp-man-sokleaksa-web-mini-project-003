const initialState = {
    categories: [],
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

    case "FETCH_CATEGORY":
        return { ...state, categories: payload }

    case "DELETE_CATEGORY":
        return { ...state, categories: state.categories.filter(item => item._id !== payload._id) }

    default:
        return state
    }
}
